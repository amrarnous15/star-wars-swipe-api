import { createRouter, createWebHistory } from 'vue-router'
import  Films from '@/views/Films.vue'
import  Starships from '@/views/Starships.vue'
export const router = createRouter({
    linkActiveClass: "active",
    history: createWebHistory(),
    routes: [
        { path: '/', name: 'Films', component: Films },
        { path: '/starships',  name: 'Starships', component: Starships }
    ]
})